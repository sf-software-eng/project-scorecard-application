from scores.models import Answer, Project, Score
import csv
from django.http import HttpResponse

class ProjectCSV():

    content_type='text/csv'

    def csv(self, projects):
        self.projects = projects
        return self.get_model_as_csv_file_response()


    def as_csv(self):
        file_name = '/tmp/projects.csv'
        with open(file_name, 'w+') as f:
            writer = csv.writer(f)
            fields = ['project', 'date', 'score percentage', 'score', 'max score',]
            
            answers = Answer.objects.filter(project = self.projects[0])
            for answer in answers:
                fields.append(answer.question.text)
            writer.writerow( fields )

            for project in self.projects:
                score, created = Score.objects.get_or_create(project = project)
                row = [project.name, project.get_created().strftime("%d-%m-%Y"),
                    score.percentage_score, score.score, score.max_score]
                answers = Answer.objects.filter(project = project)
                for answer in answers:
                    row.append(answer.choice)
                writer.writerow(row)
            path = f.name
        return path

    def get_model_as_csv_file_response(self):
        """
        Call this function from your model admin
        """
        with open(self.as_csv(), 'r') as f:
            response = HttpResponse(f.read(), content_type=self.content_type)
            response['Content-Disposition'] = 'attachment; filename=%s' % f.name.split('/')[-1:][0]
        return response