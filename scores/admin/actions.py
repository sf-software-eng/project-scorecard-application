from django.contrib import admin
from scores.admin.csv import ProjectCSV
from scores.models import Answer, Project, Score
from datetime import datetime


class ProjectActions(admin.ModelAdmin):
    actions = ['make_archive', 'download_csv', 'just_archive', 'calc_score']


    @admin.action(description='Download CSV')
    def download_csv(self, request, queryset):
        self.calc_score(request, queryset)

        meta = {
                'file': '/tmp/projects.csv',
                'queryset': queryset,
                'fields': ('name',)
            }
        return ProjectCSV().csv(queryset) 

    @admin.action(description='Archive projects')
    def just_archive(self, request, queryset):
        queryset.update(archived = True)


    @admin.action(description='Calculate Score')
    def calc_score(self, request, queryset):
        for project in queryset.all():
            score_obj, created = Score.objects.get_or_create(project = project)
            answers = project.answer_set.all()
            max_score = 0
            score = 0

            for answer in answers:
                question = answer.question 
                if question is not None:
                    print(question.base_score, ' ', question.weightage)
                    if answer.choice > 0:
                        max_score += question.base_score * question.weightage
                    score +=  answer.choice * question.weightage

            score_obj.max_score = max_score
            score_obj.score = score
            if max_score > 0:
                score_obj.percentage_score = int(round(100*score/max_score))
            else:
                score_obj.percentage_score = 0

            score_obj.save()



    @admin.action(description='Archive and replicate projects')
    def make_archive(self, request, queryset):
        self.calc_score(request, queryset)

        arr = []
        #for project in queryset.filter(created__lte=datetime.now() - timedelta(days=9))
        for project in queryset.all():
            arr.append(project)

        queryset.update(archived=True)

        for p in arr:
            answers = Answer.objects.filter(project = p)
            p.pk = None
            p.archived = False
            p.created = datetime.now()
            p.save()
            for answer in answers:
                answer.pk = None
                answer.project = p
                answer.save()