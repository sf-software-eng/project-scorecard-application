from scores.models import Answer, Project, Score, Group, Question
from django.contrib import admin
from django.db import models
from datetime import timedelta
from datetime import datetime
from scores.admin.filters import IsActiveFilter
from scores.admin.actions import ProjectActions
from django import forms

admin.site.site_header = "Skillfield PMO Admin"
admin.site.site_title = "Skillfield PMO Admin"
admin.site.index_title = "Welcome to Skillfield PMO Admin"


"""

class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        widgets = {
            'choice':forms.RadioSelect
        }
        fields = '__all__' # required for Django 3.x

"""

class AnswerInline(admin.StackedInline):
    #form = QuestionForm'
    model = Answer
    extra = 3
    fieldsets = [
        (None,      {'fields': ['group', 'question', 'choice']})
    ]


    # Can comment out or remove this method if we don't need to order by a group in the inline project answers
    def get_queryset(self, request):
        qs = super(AnswerInline, self).get_queryset(request)
        return qs.order_by('group')


class ProjectAdmin(ProjectActions):
    list_filter = [IsActiveFilter]

    fieldsets = [
        (None,      {'fields': ['name']})
    ]
    inlines = [AnswerInline]


admin.site.register(Project, ProjectAdmin)
admin.site.register(Score)
admin.site.register(Group)


admin.site.register(Question)



