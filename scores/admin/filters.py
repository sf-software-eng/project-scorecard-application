from django.contrib.admin import SimpleListFilter

try:
    from django.utils.translation import ugettext_lazy as _
except:
    from django.utils.translation import gettext_lazy as _

class IsActiveFilter(SimpleListFilter):
    title = _('Active')

    parameter_name = 'active'

    def lookups(self, request, model_admin):
        return (
            (None, _('Yes')),
            ('no', _('No')),
            ('all', _('All')),
        )

    def choices(self, cl):
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == lookup,
                'query_string': cl.get_query_string({
                    self.parameter_name: lookup,
                }, []),
                'display': title,
            }

    def queryset(self, request, queryset):
        if self.value() == 'no':
            return queryset.filter(archived=True)
        elif self.value() is None:
            return queryset.filter(archived=False)
        elif self.value() == 'all':
            return queryset