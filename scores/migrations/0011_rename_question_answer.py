# Generated by Django 4.0.2 on 2022-02-03 04:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('scores', '0010_delete_groupquestionlabel'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Question',
            new_name='Answer',
        ),
    ]
