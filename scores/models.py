from django.db import models
from django.utils import timezone

class Group(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
            return self.name


class Project(models.Model):
    name = models.CharField(max_length=200)
    archived = models.BooleanField(default=False)
    created = models.DateTimeField('Date created', auto_now_add=True, null=True)

    def get_created(self):
        return timezone.localtime(self.created)


    def __str__(self):
        
        return self.name + " (" + self.get_created().strftime("%d-%m-%Y") + ")"


class Score(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    max_score = models.FloatField(null=True)
    score = models.FloatField(null=True)
    percentage_score = models.FloatField(null=True)

    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "Score ({percent}%) - ".format(percent = self.percentage_score) + str(self.project)


class Question(models.Model):
    text = models.CharField(max_length=255)
    base_score = models.IntegerField(default = 4)
    weightage = models.IntegerField()
    created = models.DateTimeField('Date created', auto_now_add=True, null=True)

    def __str__(self):
            return  self.text


class Answer(models.Model):
    
    option_choices = [
        (1, 'NOT STARTED (pending)'),
        (2, 'STARTED (under development)'),
        (3, 'COMPLETED (developed & communicated)'),
        (4, 'MANAGED (periodically reviewed, changes managed)'),
        (0, 'N/A'),
    ]

    project = models.ForeignKey(Project, on_delete=models.DO_NOTHING)
    group = models.ForeignKey(Group, on_delete=models.DO_NOTHING)
    question = models.ForeignKey(Question, on_delete=models.DO_NOTHING, null=True)
    created = models.DateTimeField('Date created', auto_now_add=True, null=True)
    choice = models.IntegerField(choices=option_choices)

    def choice_str(self):
        for ind, txt in self.option_choices:
            if ind == self.choice:
                return txt
        return "Unkonw choice"

    def __str__(self):
            if self.question:
                return self.question.text + " " + self.choice_str()
            else:
                return  "Question unavailable"



"""

question_list = [{
            'group': 'COMMS',
            'questions': [
                'Project Cadence Established',
                'Roles & Responsibilities Defined',
                'Project Working Group implemented',
                'Project Control Group implemented',
                'Regular Status Report'
            ],
        },

        {
            'group': 'COMMS',
            'questions': [
                'Project Cadence Established',
                'Roles & Responsibilities Defined',
                'Project Working Group implemented',
                'Project Control Group implemented',
                'Regular Status Report'
            ],
},

]
"""



"""
class Option(models.Model):
    option_choices = [
        (1, 'NOT STARTED (pending)'),
        (2, 'STARTED (under development)'),
        (3, 'COMPLETED (developed & communicated)'),
        (4, 'MANAGED (periodically reviewed, changes managed)'),
        (5, 'N/A'),
    ]
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice = models.IntegerField(choices=option_choices)

    def __str__(self):
        for ind, txt in self.option_choices:
            if ind == self.choice:
                return txt
        return "Unkonw choice"

"""

